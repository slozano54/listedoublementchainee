#!/usr/bin/python3
#-*- coding: utf8 -*-

# @author : Sébastien LOZANO

"""
## Programme principal

Permet de lancer les différents tests de la partie1 et de lapartie2 du projet.   
"""
pass

# Pour mesurer le temps de traitement du script
from datetime import datetime 
# Pour copier les variable
import copy
# Pour les commandes systeme
import os 

# # Module ldc
from ldc.partie1 import *
from ldc.partie2 import *
if __name__ == "__main__":
    from ldc.tools import *

# Script principal
def main():
    """Procédure principale du programme principal"""    
    # Affichages de sorties
    os.system("clear")
    myTitle("PROJET LDC BLOC3")
    choice = ''
    while choice not in ['1','2','3']:                
        choice = input("""Quels tests voulez-vous lancer ?
        ---> 1 : TESTS DE LA PARTIE1
        ---> 2 : TESTS DE LA PARTIE2
        """)

    if (choice == '1'):        
        tests_MainP1()
    elif (choice == '2'):        
        tests_MainP2()

    if __name__ == "__main__":
        restart = ''
        while restart not in ['o','n']:
            genericTitle("UN AUTRE TEST ?",'#')
            restart = input("Taper o ou n pour relancer le script --> ")
            if restart == 'o':
                main()  
 
if __name__ == "__main__":
    main()    