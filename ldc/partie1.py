#!/usr/bin/python3
#-*- coding: utf8 -*-

# @author : Sébastien LOZANO

"""
Toutes les méthodes permettant de travailler sur les listes doublement chainées
"""
pass


# Pour mesurer le temps de traitement du script
from datetime import datetime
# Pour les commandes systeme
import os

from tools import *

#############################################################################################
#              DEFINITIONS
#############################################################################################
def creerListeVide():
    """Crée une liste vide."""
    pass
    ## la liste vide vaut None 
    return None

def creerNoeudLDC(val:int,precedent:list,suivant:list)->list:
    """Crée un Noeud.

    Un noeud est une liste Python de trois cases **[valeur, noeud precedent, noeud suivant]**
    
    **Paramètres**

        * val -- la valeur du noeud.
        * precedent -- le noeud précédent. None en début de liste.
        * suivant -- le noeud suivant. None en fin de liste.

    **Sorties**

        * Une liste de trois éléments [val,precedent,suivant].
    
    """
    pass
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")
    assert isinstance(precedent,list) or precedent==None,warnString(str(precedent)+" devrait être une liste ou None")
    assert isinstance(suivant,list) or suivant==None,warnString(str(suivant)+" devrait être une liste ou None")
     
    return [val,precedent,suivant]

def estNoeudLDC_Droite(n:list)->bool:
    """Vérifie que **n** est un noeud de liste doublement chainée et que tous les éléments à droite de **n** le sont aussi.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.

    **Sorties**

        * Un booléen    
    """
    pass
    assert (n==None) or (isinstance(n,list) and len(n)==3 and isinstance(n[0],int) and estNoeudLDC_Droite(n[1])),warnString(str(n)+" devrait être None ou une liste de 3 éléments [entier, noeudLDC ou None, noeudLDC ou None]")
    return True

def estNoeudLDC_Gauche(n:list)->bool:
    """Vérifie que **n** est un noeud de liste doublement chainée et que tous les éléments à gauche de **n** le sont aussi.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.

    **Sorties**

        * Un booléen    
    """
    pass
    assert (n==None) or (isinstance(n,list) and len(n)==3 and isinstance(n[0],int) and estNoeudLDC_Gauche(n[2])),warnString(str(n)+" devrait être None ou une liste de 3 éléments [entier, noeudLDC ou None, noeudLDC ou None]")
    return True

def estNoeudLDC(n:list)->bool:
    """Vérifie que **n** est un noeud de liste doublement chainée à l'aide des fonctions auxiliaires **estNoeudDroiteLDC()** et **estNoeudGaucheLDC()** ci-dessous.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.
    
    **Sorties**

        * Un booléen.
    """
    pass
    assert (n==None) or (estNoeudLDC_Droite(n) and estNoeudLDC_Gauche(n)),warnString(str(n)+" devrait être None ou une liste de 3 éléments [entier, noeudLDC ou None, noeudLDC ou None]")
    return True

#############################################################################################
#              GETTERS
#############################################################################################
def getValLDC(n:list)->int:
    """Récupère la valeur du noeud **n** de liste doublement chainée.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.
    
    **Sorties**

        * La valeur du noeud.
    """    
    pass
    assert n is not None,warnString("La liste vide n'a pas de valeur")      
    assert estNoeudLDC(n),warnString("La liste doit être un noeud LDC")
    return n[0]

def getPrecedentLDC(n:list)->list:
    """Récupère le noeud précédent du noeud **n**.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée  .

    **Sorties**

        * Le noeud précédent.
    """    
    pass 
    assert (n is not None),warnString("La liste doit être un vrai noeud et pas None pour avoir un noeud précédent")
    assert estNoeudLDC(n),warnString("La liste doit être un noeud LDC")
    return n[1]

def getSuivantLDC(n:list)->list:
    """Récupère le noeud suivant  du noeud **n**.
      
    **Paramètres**

        * n -- noeud de liste doublement chainée.    

    **Sorties**

        * Le noeud suivant.
    """    
    pass     
    assert (n is not None),warnString("La liste doit être un vrai noeud et pas None pour avoir un noeud suivant")
    assert estNoeudLDC(n), warnString("La liste doit être un noeud LDC")
    return n[2]

#############################################################################################
#              SETTERS
#############################################################################################

def setValLDC(n:list,val:int)->int:
    """Modifie/Attribue la valeur **val** du/au noeud **n** d'une liste doublement chainée.

    **Paramètres**

        * n -- noeud de liste doublement chainée.
        * val -- valeur à attribuer au noeud de la liste doublement chainée.
    """    
    pass     
    assert (n is not None),warnString("La liste doit être un vrai noeud et pas None pour avoir une valeur")
    assert estNoeudLDC(n),warnString("La liste doit être un noeud LDC")
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")
    n[0] = val

def setPrecedentLDC(n:list,precedent:list)->list:
    """Modifie/Attribue le noeud **precedent** du noeud **n** d'une liste doublement chainée.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.    
        * precedent -- le noeud précédent à attribuer. None en début de liste.
    """    
    pass 
    assert (n is not None),warnString("La liste doit être un vrai noeud et pas None pour avoir un noeud précédent")
    assert (estNoeudLDC(n)) # msg issu de estNoeudLDC 
    assert (estNoeudLDC(precedent)) # msg issu de estNoeudLDC 
    assert isinstance(precedent,list) or precedent==None # msg issu de estNoeudLDC 
    # Le precedent de n est maintenant precedent
    n[1] = precedent
    
def setSuivantLDC(n:list,suivant:list)->list:
    """Modifie/Attribue le noeud **suivant** du noeud **n** d'une liste doublement chainée.
    
    **Paramètres**

        * n -- noeud de liste doublement chainée.
        * suivant -- le noeud suivant à attribuer. None en fin de liste.    
    """    
    pass     
    assert (n is not None),warnString("La liste doit être un vrai noeud et pas None pour avoir un noeud suivant")
    assert (estNoeudLDC(n)),warnString("La liste doit être un noeud LDC")
    assert (estNoeudLDC(suivant)),warnString("Le noeud suivant doit être un noeud LDC")
    assert isinstance(suivant,list) or suivant==None,warnString("suivant doit être une liste ou None")
    n[2] = suivant

#############################################################################################
#              FONCTIONS
#############################################################################################
def longueurLDC(ldc:list)->int:
    """Renvoie la longueur de la liste doublement chainée **ldc**.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.

    **Sorties**

        * Un entier avec le nombre d'éléments de la liste doublement chainée. 0 si None.
    
    """
    pass
    assert (ldc == None) or estNoeudLDC(ldc[0])# msg issu de estNoeudLDC - Fonction recursive donc on ne vérifie que le premier element.
    # 
    longueur = 0
    if ldc == None:
        longueur = 0        
    elif len(ldc) == 1:
        longueur = 1
    else:
        longueur = 1+longueurLDC(ldc[1:])
    return longueur

def insererDebutLDC(ldc:list,val:int)->list:
    """Insère un noeud avec la valeur **val** au début de la liste doublement chainée **ldc**.    
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.
        * val -- la valeur du noeud à insérer

    **Sorties**

        * La liste doublement chainée modifiée avec le noeud supplémentaire au début.
    
    """
    pass
    assert (ldc is None) or (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC 
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")    
    # On crée un nouveau noeud avec la valeur val
    newNode = creerNoeudLDC(val,None,None)
    # Si c'était la liste vide la nouvelle liste correspond au nouveau noeud    
    if (ldc is None):
        return newNode        
    # Sinon on insert le nouveau noeud au début de la liste
    else:
        # Le noeud precedent de newNode reste None,
        # Le noeud suivant de newNode est le premier noeud de la liste
        setSuivantLDC(newNode,ldc[0])
        # On modifie le noeud precedent du premier noeud de la liste
        setPrecedentLDC(ldc[0],newNode)
        # On ajoute le nouveau noeud à la liste
        ldc.insert(0,newNode)
        return ldc

def insererFinLDC(ldc:list,val:int)->list:
    """Insère un noeud avec la valeur **val** à la fin de la liste doublement chainée **ldc**.    
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.
        * val -- la valeur du noeud à insérer

    **Sorties**

        * La liste doublement chainée modifiée avec le noeud supplémentaire à la fin.
    
    """
    pass
    assert (ldc is None) or (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC 
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")    
    # On crée un nouveau noeud avec la valeur val
    newNode = creerNoeudLDC(val,None,None)
    # Si c'était la liste vide la nouvelle liste correspond au nouveau noeud    
    if (ldc is None):
        return newNode        
    # Sinon on insert le nouveau noeud à la fin de la liste
    else:
        # Le noeud suivant de newNode reste None,
        # Le noeud precedent de newNode est le dernier noeud de la liste
        setPrecedentLDC(newNode,ldc[-1])
        # On modifie le noeud suivant du dernier noeud de la liste
        setSuivantLDC(ldc[-1],newNode)
        # On ajoute le nouveau noeud à la liste
        ldc.insert(len(ldc),newNode)
        return ldc     

def supprimerDebutLDC(ldc:list)->list:
    """Supprime le premier noeud d'une liste doublement chainée **ldc**.    
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.        

    **Sorties**

        * La liste doublement chainée modifiée dont le premier noeud a été supprimé.
    
    """
    pass
    assert ldc is not None,warnString("On ne peut supprimer le premier élément de la liste vide")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC 
    # On supprime le premier noeud au début de la liste
    ldc.pop(0)        
    # Le noeud precedent du nouveau premier noeud est None
    setPrecedentLDC(ldc[0],None)
    return ldc
    
def supprimerFinLDC(ldc:list)->list:
    """Supprime le dernier noeud d'une liste doublement chainée **ldc**.    
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.        

    **Sorties**

        * La liste doublement chainée modifiée dont le dernier noeud a été supprimé.
    
    """
    pass
    assert ldc is not None,warnString("On ne peut supprimer le dernier élément de la liste vide")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC 
    # On supprime le dernier noeud à la fin de la liste
    ldc.pop()        
    # Le noeud suivant du nouveau dernier noeud est None
    setSuivantLDC(ldc[-1],None)
    return ldc

def accesLDC(ldc:list,p:int)->list:
    """Renvoie le noeud d'indice p de la liste doubelement chainée **ldc**.
    Le premier noeud a pour indice 0.

    **Paramètres**

        * ldc -- liste doublement chainée.
        * p -- indice de la position du noeud à récupérer.

    **Sorties**

        * Un noeudLDC
    """
    pass
    assert ldc is not None,warnString("On ne peut accéder à un élément de la liste vide")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC 
    assert isinstance(p,int),warnString(str(p)+" devrait être un entier")
    assert p in range(len(ldc)),warnString(str(p)+", la position du noeud doit être comprise entre 0 et "+str(len(ldc)-1))
    # On renvoit le noeud d'indice p
    return ldc[p]

def insererPlaceLDC(ldc:list,val:int,p:int)->list:
    """Insère le noeud contenant la valeur **val** juste après le noeud d'indice **p** dans la liste doublement chainée **ldc**;

    Le premier noeud a pour indice 0.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.
        * val -- valeur du noeud à insérer.
        * p -- indice de la position  précédent l'insertion.
    
    **Sorties**
        
        * La liste doublement chainée modifiée avec le noeud supplémentaire.
    """    
    pass
    assert (ldc is None) or (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")
    assert isinstance(p,int),warnString(str(p)+" devrait être un entier")
    assert (ldc is None and p==0) or (p in range(len(ldc))),warnString(str(p)+", la position du noeud doit être comprise entre 0 et "+str(len(ldc)-1))
    # On crée un nouveau noeud avec la valeur val
    newNode = creerNoeudLDC(val,None,None)
    # Si c'était la liste vide la nouvelle liste correspond au nouveau noeud    
    if (ldc is None):
        return newNode        
    # Sinon on insert le nouveau noeud à l'indice p
    else:
        # Le noeud precedent de newNode est l[p],        
        setPrecedentLDC(newNode,ldc[p])        
        # Le noeud suivant de l[p] est newNode,
        setSuivantLDC(ldc[p],newNode)
        if p in range(len(ldc)-1):
            # Le noeud suivant de newNode l[p+1] Ou None
            setSuivantLDC(newNode,ldc[p+1])
            # Le noeud precedent de l[p+1] est newNode,
            setPrecedentLDC(ldc[p+1],newNode)
        # On ajoute le nouveau noeud à la liste
        ldc.insert(p+1,newNode)
        return ldc

def supprimerPlaceLDC(ldc:list,p:int)->list:
    """Supprime le noeud d'indice **p** de la liste doublement chainée **ldc**;

    Le premier noeud a pour indice 0.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.        
        * p -- indice de la position  à supprimer.
    
    **Sorties**
        
        * La liste doublement chainée modifiée avec le noeud supplémentaire.
    """    
    pass
    assert ldc is not None,warnString("On ne peut rien supprimer de la liste vide")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC    
    assert isinstance(p,int),warnString(str(p)+" devrait être un entier")
    assert p in range(len(ldc)),warnString(str(p)+", la position du noeud doit être comprise entre 0 et "+str(len(ldc)-1))
    # Selon la position à supprimer on modifie les liaisons
    if p == 0:
        setPrecedentLDC(ldc[p+1],None)
    elif p == len(ldc)-1:
        setSuivantLDC(ldc[p-1],None)
    else:
        setPrecedentLDC(ldc[p+1],ldc[p-1])
        setSuivantLDC(ldc[p-1],ldc[p+1])
    # Dans tous les cas, on supprimme ldc[p]
    ldc.pop(p)
    return ldc

def supprimerValLDC(ldc:list,val:int)->list:
    """Supprime le premier noeud de valeur **val** de la liste doublement chainée **ldc**;

    Le premier noeud a pour indice 0.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.        
        * val -- valeur à supprimer.
    
    **Sorties**
        
        * La liste doublement chainée modifiée avec le premier noeud de valeur **val** en moins.
    """    
    pass
    assert ldc is not None,warnString("On ne peut rien supprimer de la liste vide")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC    
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")
    # On parcourt la liste pour trouver le premier noeud de valeur val
    p = 0
    while getValLDC(ldc[p]) != val and p != len(ldc)-1:
        p += 1
    # Si p vaut la longueur de la list alors c'est que le valeur n'y figure pas
    # On ne supprime que dans le cas contraire
    if (p != len(ldc)-1):
        ldc = supprimerPlaceLDC(ldc,p)      
    return ldc

def LDC2List(ldc:list)->list:
    """Renvoie la liste Python contenant les éléments de **ldc**;
    Renvoie **[]** si **ldc** vaut **None**
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.                
    
    **Sorties**
        
        * Les éléments de la liste doublement chainée dans un tableau.
    """    
    pass    
    assert (ldc is None) or (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC        
    # On parcourt la liste et on  récupère les valeurs dans l'ordre
    valsLDC = []
    if ldc is not None:
        for noeud in ldc:
            valsLDC.append(getValLDC(noeud))
    return valsLDC

def list2LDC(listPython:list)->list:
    """Renvoie la liste doublement chainée contenant dans le même ordre les éléments de la liste Python **listPython**;

    Renvoie **None** si **listePython** vaut **[]**
    
    **Paramètres**
    
        * listePython -- liste Python.                
    
    **Sorties**
        
        * Une liste doublement chainée avec les éléments du tableau passé en paramètre.
    """    
    pass
    assert isinstance(listPython,list),warnString(str(listPython)+" devrait être une liste")
    assert (all(isinstance(element,int) for element in listPython)),warnString("Tous les éléments de "+str(listPython)+" devrait être des entiers") # msg issu de estNoeudLDC        
    outLDC = []
    if listPython == []:
        outLDC = None
    else: # listPython != [] : 
        outLDC.append(creerNoeudLDC(listPython[0],None,None))                 
        for indice in range(1,len(listPython)):                                    
            outLDC.append(creerNoeudLDC(listPython[indice],outLDC[indice-1],None))
            setSuivantLDC(outLDC[indice-1],outLDC[indice])       
    return outLDC        

#############################################################################################
#              TESTS
#############################################################################################
def tests_Definitions():
    """Teste les définitions"""            
    # pour tester les assertions de estNoeudLDC() et de getValLDC()
    listeVide = creerListeVide()                
    noeudOK = creerNoeudLDC(5,None,None)   
    noeudKO = ['foo',None,None]     

    myTitle("TESTS DEFINITIONS")
    myTestsTitle("creerNoeudLDC()")
    
    print("creerNoeudLDC(7,None,None)  : ", creerNoeudLDC(7,None,None))
    myAssertTestsTitle("estNoeudLDC() -> test des assertions")    
    try:
        creerNoeudLDC('foo',None,None)
    except AssertionError as msg:
        print(warnString("creerNoeudLDC('foo',None,None) -> "),msg)         
    try:
        creerNoeudLDC(1,'bar',None)
    except AssertionError as msg:
        print(warnString("creerNoeudLDC(1,'bar',None)    -> "),msg)         
    try:
        creerNoeudLDC(1,None,'bar')
    except AssertionError as msg:
        print(warnString("creerNoeudLDC(1,None,'bar')    -> "),msg)         

    myTestsTitle("estNoeudLDC()")       
    print("Liste vide : ",estNoeudLDC(listeVide))
    print("Noeud OK   : ",estNoeudLDC(noeudOK))

    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("estNoeudLDC() -> test des assertions")    
    try:
        print(warnString("noeudKO  : "+str(noeudKO)))
        estNoeudLDC(noeudKO)
    except AssertionError as msg:
        print(warnString("estNoeudLDC(noeudKO) -> "),msg)    

def tests_Getters():
    """Teste les getters"""            
    # pour tester les assertions de getValLDC()
    listeVide = creerListeVide()                
    noeudOK = creerNoeudLDC(5,None,None)   
    noeudKO = ['foo',None,None]

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    

    myTitle("TESTS GETTERS")

    myTestsTitle("Les noeuds")
    
    print("noeud0 : ",noeud0)
    print("estNoeudLDC(noeud0) ?    : ",estNoeudLDC(noeud0))
    print("noeud1 : ",noeud1)
    print("estNoeudLDC(noeud1) ?    : ",estNoeudLDC(noeud1))
    print("noeudOK : ",noeudOK)
    print("estNoeudLDC(noeudOK) ?    : ",estNoeudLDC(noeudOK))

    myTestsTitle("getValLDC()")
    print("getValLDC(noeud0) : ",getValLDC(noeud0))
    print("getValLDC(noeud1) : ",getValLDC(noeud1))
    print("getValLDC(noeudOK) : ",getValLDC(noeudOK))
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("getValLDC() -> test des assertions")    
    try:
        getValLDC(listeVide)
    except AssertionError as msg:
        print(warnString("getValLDC(listeVide) -> "),msg)
    try:
        print(warnString("noeudKO    : "+str(noeudKO)))
        getValLDC(noeudKO)
    except AssertionError as msg:
        print(warnString("getValLDC(noeudKO)   ->"),msg)

    myTestsTitle("getPrecedentLDC()")      
    print("getPrecedentLDC(noeud0) : ",getPrecedentLDC(noeud0))
    print("getPrecedentLDC(noeud1) : ",getPrecedentLDC(noeud1))
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("getPrecedentLDC() -> test des assertions")    
    try:
        getPrecedentLDC(listeVide)
    except AssertionError as msg:
        print(warnString("getPrecedentLDC(listeVide) -> "),msg)
    print(sepSymb('-'))

    myTestsTitle("getSuivantLDC()")    
    print("getSuivantLDC(noeud0) : ",getSuivantLDC(noeud0))
    print("getSuivantLDC(noeud1) : ",getSuivantLDC(noeud1))
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("getSuivantLDC() -> test des assertions")    
    try:
        getSuivantLDC(listeVide)
    except AssertionError as msg:
        print(warnString("getSuivantLDC(listeVide) -> "),msg)
    
def tests_Setters():    
    """Teste les setters"""
    # pour tester les assertions de getValLDC()
    listeVide = creerListeVide() 
    noeudOK = creerNoeudLDC(5,None,None)                      
    noeudKO = ['foo',None,None]

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    

    noeudTestSetters = creerNoeudLDC(9,None,None)
    
    myTitle("SETTERS")

    myTestsTitle("Les noeuds d'origine")    
    print("noeud0               : ",noeud0)
    print("noeud1               : ",noeud1)
    print("noeud2               : ",noeud2)

    myTestsTitle("setValLDC()")
    print("noeud0               : ",noeud0)
    setValLDC(noeud0,10)
    print("setValLDC(noeud0,10) : ",noeud0)
    print("noeud1               : ",noeud1)
    setValLDC(noeud1,11)
    print("setValLDC(noeud1,11) : ",noeud1)
    print("noeud2               : ",noeud2)
    setValLDC(noeud2,12)    
    print("setValLDC(noeud2,12) : ",noeud2)
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("setValLDC() -> test des assertions")    
    try:
        setValLDC(listeVide,2)
    except AssertionError as msg:
        print(warnString("setValLDC(listeVide,2)  ->"),msg)        
    try:
        print(warnString("noeudKO    : "+str(noeudKO)))
        setValLDC(noeudKO,2)
    except AssertionError as msg:
        print(warnString("setValLDC(noeudKO,2)    ->"),msg)   
    try:
        setValLDC(noeudOK,'bar')
    except AssertionError as msg:
        print(warnString("setValLDC(noeudOK,'bar')  ->"),msg)        

    myTestsTitle("setPrecedentLDC(noeudTestSetters,[99,None,None])")            
    print("noeudTestSetters avant                  :",noeudTestSetters)    
    setPrecedentLDC(noeudTestSetters,[99,None,None])
    print("noeudTestSetters après                  :",noeudTestSetters)    
    print(sepEqual("reinitialisation des valeurs"))
    setPrecedentLDC(noeudTestSetters,None)
    print("noeudTestSetters réinitialisé           :",noeudTestSetters
    )
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("setPrecedentLDC() -> test des assertions")    
    try:
        setPrecedentLDC(listeVide,noeudOK)
    except AssertionError as msg:
        print(warnString("setPrecedentLDC(listeVide,noeudOK)         ->"),msg)        
    try:
        print(warnString("noeudKO    : "+str(noeudKO)))
        setPrecedentLDC(noeudTestSetters,noeudKO)
    except AssertionError as msg:
        print(warnString("setPrecedentLDC(noeudTestSetters,noeudKO)  ->"),msg)   
    try:
        print(warnString("noeudKO    : "+str(noeudKO)))
        setPrecedentLDC(noeudKO,noeudOK)
    except AssertionError as msg:        
        print(warnString("setPrecedentLDC(noeudKO,noeudOK)           ->"),msg)    
    try:
        setPrecedentLDC(noeudTestSetters,'bar')
    except AssertionError as msg:
        print(warnString("setPrecedentLDC(noeudTestSetters,'bar')      ->"),msg)    

    myTestsTitle("setSuivantLDC(noeudTestSetters,[99,None,None])")    
    setSuivantLDC(noeudTestSetters,[99,None,None])
    print("noeudTestSetters après                  :",noeudTestSetters)    
    print(sepEqual("reinitialisation des valeurs"))
    setSuivantLDC(noeudTestSetters,None)
    print("noeudTestSetters réinitialisé           :",noeudTestSetters)    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("setSuivantLDC() -> test des assertions")    
    try:
        setSuivantLDC(listeVide,noeudOK)
    except AssertionError as msg:
        print(warnString("setSuivantLDC(listeVide,noeudOK)         ->"),msg)        
    try:
        setSuivantLDC(noeudTestSetters,noeudKO)
    except AssertionError as msg:
        print(warnString("setSuivantLDC(noeudTestSetters,noeudKO)  ->"),msg)   
    try:
        print(warnString("noeudKO    : "+str(noeudKO)))
        setSuivantLDC(noeudKO,noeudOK)
    except AssertionError as msg:
        print(warnString("setSuivantLDC(noeudKO,noeudOK)           ->"),msg)    
    try:
        setSuivantLDC(noeudTestSetters,'bar')
    except AssertionError as msg:
        print(warnString("setSuivantLDC(noeudTestSetters,'bar')      ->"),msg)    

def tests_longueurLDC():    
    """Teste la fonction longueurLDC()"""            
    
    listeVide = creerListeVide()                
    
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]
    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 
    
    myTestsTitle("longueurLDC()")
    print("Liste Vide              : ",listeVide)
    print("longueurLDC(listeVide)  : ",longueurLDC(listeVide))
    print("Liste ldc               : ",ldc)
    print("longueurLDC(ldc)        : ",longueurLDC(ldc))

    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("longueurLDC() -> test des assertions")    
    try:
        print(warnString("ldcKO               ->"+str(ldcKO)))
        longueurLDC(ldcKO)
    except AssertionError as msg:
        print(warnString("longueurLDC(ldcKO)  ->"),msg)   
    
def tests_insererDebutLDC():    
    """Teste la fonction insererDebutLDC()"""            

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2] 
    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 
    
    myTestsTitle("insererDebutLDC()")
    print("Liste Vide                                      : ",listeVide)    
    listeVide = insererDebutLDC(listeVide,999)
    print("Liste Vide après insererDebutLDC(listeVide,999) : ",listeVide)
    print(sepSymb("-"))
    print("Liste ldc avant                                 : ",ldc)
    ldc = insererDebutLDC(ldc,999)    
    print("Liste ldc après insererDebutLDC(ldc,999)        : ",ldc)
    # réinitialisation de ldc
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]

    # Tests pour lever les erreurs d'assertion    
    myAssertTestsTitle("insererDebutLDC() -> test des assertions")
    try:
        print(warnString("ldcKO                         ->"+str(ldcKO)))
        ldcKO = insererDebutLDC(ldcKO,999)
    except AssertionError as msg:
        print(warnString("insererDebutLDC(ldcKO,999)    ->"),msg)
    print(warnString(sepSymb("-")))   
    try:
        print(warnString("ldc                           ->"+str(ldc)))
        ldc = insererDebutLDC(ldc,'string')
    except AssertionError as msg:
        print(warnString("insererDebutLDC(ldc,'string') ->"),msg) 

def tests_insererFinLDC():    
    """Teste la fonction insererFinLDC()"""            

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2] 

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("insererFinLDC()")
    print("Liste Vide                                     : ",listeVide)    
    listeVide = insererFinLDC(listeVide,888)
    print("Liste Vide après insererFinLDC(listeVide,888)  : ",listeVide)
    print(sepSymb("-"))
    print("Liste ldc avant                                : ",ldc)
    ldc = insererFinLDC(ldc,888)    
    print("Liste ldc après insererFinLDC(ldc,888)         : ",ldc)
    # réinitialisation de ldc
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]

    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("insererFinLDC() -> test des assertions")
    try:
        print(warnString("ldcKO                         ->"+str(ldcKO)))
        ldcKO = insererDebutLDC(ldcKO,888)
    except AssertionError as msg:
        print(warnString("insererFinLDC(ldcKO,888)      ->"),msg)   
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldc                           ->"+str(ldc)))
        ldc = insererDebutLDC(ldc,'string')
    except AssertionError as msg:
        print(warnString("insererFinLDC(ldc,'string')   ->"),msg) 

def tests_supprimerDebutLDC():    
    """Teste la fonction supprimerDebutLDC()"""            

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2] 

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("supprimerDebutLDC()")
    print("Liste ldc avant                          : ",ldc)
    ldc = supprimerDebutLDC(ldc)    
    print("Liste ldc après supprimerDebutLDC(ldc)   : ",ldc)
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("supprimerDebutLDC() -> test des assertions")
    try:
        listeVide = supprimerDebutLDC(listeVide)
    except AssertionError as msg:
        print(warnString("supprimerDebutLDC(listeVide)  ->"),msg)   
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                         ->"+str(ldcKO)))
        ldcKO = supprimerDebutLDC(ldcKO)
    except AssertionError as msg:
        print(warnString("supprimerDebutLDC(ldcKO)      ->"),msg) 

def tests_supprimerFinLDC():    
    """Teste la fonction supprimerFinLDC()"""            

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2]  

    myTestsTitle("supprimerFinLDC()")    
    print("Liste ldc avant                        : ",ldc)
    ldc = supprimerFinLDC(ldc)    
    print("Liste ldc après supprimerFinLDC(ldc)   : ",ldc)
    
    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("supprimerFinLDC() -> test des assertions")
    try:
        listeVide = supprimerFinLDC(listeVide)
    except AssertionError as msg:
        print(warnString("supprimerFinLDC(listeVide)  ->"),msg)   
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                       ->"+str(ldcKO)))
        ldcKO = supprimerFinLDC(ldcKO)
    except AssertionError as msg:
        print(warnString("supprimerFinLDC(ldcKO)      ->"),msg) 

def tests_accesLDC():
    """Teste la fonction accessLDC()"""

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("accesLDC()")
    print("Liste ldc                : ",ldc)    
    print("accesLDC(ldc,0)          : ",accesLDC(ldc,0))
    print("accesLDC(ldc,1)          : ",accesLDC(ldc,1))
    print("accesLDC(ldc,2)          : ",accesLDC(ldc,2))

    # Pour tester les assertions
    myAssertTestsTitle("accesLDC() -> test assertion")    
    try:
        accesLDC(listeVide,3)
    except AssertionError as msg:
        print(warnString("accesLDC(listeVide,3)  ->"),msg)  
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                  ->"+str(ldcKO)))
        accesLDC(ldcKO,3)
    except AssertionError as msg:
        print(warnString("accesLDC(ldcKO,3)      ->"),msg)
    try:
        accesLDC(ldc,'foo')
    except AssertionError as msg:
        print(warnString("accesLDC(ldc,'foo')    ->"),msg)  
    # p doit faire partie des indices possibles pour la liste ldc, ici max 2
    try:
        accesLDC(ldc,3)
    except AssertionError as msg:
        print(warnString("accesLDC(ldc,3)        ->"),msg)

def tests_insererPlaceLDC():
    """Teste la fonction inserePlaceLDC()"""

    listeVide = creerListeVide() 

    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("insererPlaceLDC()")    
    print("Liste Vide                   : ",listeVide)    
    listeVide = insererPlaceLDC(listeVide,888,0)
    print("Liste Vide après             : ",listeVide)
    print("Liste ldc                       : ",ldc)    
    ldc=insererPlaceLDC(ldc,999,0)
    print("insererPlaceLDC(ldc,999,0)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]    
    ldc=insererPlaceLDC(ldc,999,1)
    print("insererPlaceLDC(ldc,999,1)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2]    
    ldc=insererPlaceLDC(ldc,999,2)
    print("insererPlaceLDC(ldc,999,2)      : ",ldc)
    # On réinitialise pour les tests d'assertions
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldc = [noeud0,noeud1,noeud2] 

    # Pour tester les assertions
    myAssertTestsTitle("insererPlaceLDC() -> test assertion")    
    try:
        print(warnString("ldcKO                          ->"+str(ldcKO)))
        insererPlaceLDC(ldcKO,999,0)
    except AssertionError as msg:
        print(warnString("insererPlaceLDC(ldcKO,999,0)   ->"),msg)  
    print(warnString(sepSymb("-")))
    try:
        insererPlaceLDC(ldc,'foo',0)
    except AssertionError as msg:
        print(warnString("insererPlaceLDC(ldc,'foo',0)   ->"),msg)  
    try:
        insererPlaceLDC(ldc,999,'bar')
    except AssertionError as msg:
        print(warnString("insererPlaceLDC(ldc,999,'bar') ->"),msg)  
    try:
        insererPlaceLDC(ldc,999,3)
    except AssertionError as msg:
        print(warnString("insererPlaceLDC(ldc,999,3)     ->"),msg)  

def tests_supprimerPlaceLDC():
    """Teste la fonction supprimerPlaceLDC()"""

    listeVide = creerListeVide() 
    
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2) 
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 
     
    myTestsTitle("supprimerPlaceLDC()")    
    print("Liste ldc                     : ",ldc)    
    print(sepSymb("-"))
    ldc=supprimerPlaceLDC(ldc,0)
    print("supprimerPlaceLDC(ldc,0)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]
    ldc=supprimerPlaceLDC(ldc,1)
    print("supprimerPlaceLDC(ldc,1)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]
    ldc=supprimerPlaceLDC(ldc,2)
    print("supprimerPlaceLDC(ldc,2)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]
    ldc=supprimerPlaceLDC(ldc,3)
    print("supprimerPlaceLDC(ldc,3)      : ",ldc)
    # On réinitialise pour les tests d'assertions
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]

    # Pour tester les assertions
    myAssertTestsTitle("supprimerPlaceLDC() -> test assertion")    
    try:
        supprimerPlaceLDC(listeVide,0)
    except AssertionError as msg:
        print(warnString("supprimerPlaceLDC(listeVide,0)  ->"),msg)  
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                           ->"+str(ldcKO)))
        supprimerPlaceLDC(ldcKO,0)
    except AssertionError as msg:
        print(warnString("supprimerPlaceLDC(ldcKO,0)      ->"),msg)
    print(warnString(sepSymb("-")))  
    try:
        print(warnString("ldc                             ->"+str(ldc)))
        supprimerPlaceLDC(ldc,'foo')
    except AssertionError as msg:
        print(warnString("supprimerPlaceLDC(ldc,'foo')    ->"),msg)  
    try:
        supprimerPlaceLDC(ldc,4)
    except AssertionError as msg:
        print(warnString("supprimerPlaceLDC(ldc,4)        ->"),msg)  

def tests_supprimerValLDC():
    """Teste la fonction supprimerValLDC()"""

    listeVide = creerListeVide() 
    
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2) 
    noeud1bis = creerNoeudLDC(1,noeud2,None)
    setSuivantLDC(noeud2,noeud1bis)   
    ldc = [noeud0,noeud1,noeud2,noeud1bis]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("supprimerValLDC()")    
    print("Liste ldc                     : ",ldc)    
    print(sepSymb("-"))
    ldc=supprimerPlaceLDC(ldc,0)
    print("supprimerValLDC(ldc,0)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)
    noeud1bis = creerNoeudLDC(1,noeud2,None)
    setSuivantLDC(noeud2,noeud1bis)   
    ldc = [noeud0,noeud1,noeud2,noeud1bis]
    ldc=supprimerValLDC(ldc,1)
    print("supprimerValLDC(ldc,1)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud1bis = creerNoeudLDC(1,noeud2,None)
    setSuivantLDC(noeud2,noeud1bis)   
    ldc = [noeud0,noeud1,noeud2,noeud1bis]
    ldc=supprimerValLDC(ldc,2)
    print("supprimerValLDC(ldc,2)      : ",ldc)
    # On réinitialise
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud1bis = creerNoeudLDC(1,noeud2,None)
    setSuivantLDC(noeud2,noeud1bis)   
    ldc = [noeud0,noeud1,noeud2,noeud1bis]
    ldc=supprimerValLDC(ldc,3)
    print("supprimerValLDC(ldc,3)      : ",ldc)
    # On réinitialise pour les tests d'assertions
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    noeud1bis = creerNoeudLDC(1,noeud2,None)
    setSuivantLDC(noeud2,noeud1bis)   
    ldc = [noeud0,noeud1,noeud2,noeud1bis]

    # Pour tester les assertions
    myAssertTestsTitle("supprimerPlaceLDC() -> test assertion")
    try:
        supprimerValLDC(listeVide,0)
    except AssertionError as msg:
        print(warnString("supprimerValLDC(listeVide,0) ->"),msg)
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                        ->"+str(ldcKO)))
        supprimerValLDC(ldcKO,0)
    except AssertionError as msg:
        print(warnString("supprimerValLDC(ldcKO,0)     ->"),msg)  
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldc                          ->"+str(ldc)))
        supprimerValLDC(ldc,'foo')
    except AssertionError as msg:
        print(warnString("supprimerValLDC(ldc,'foo')   ->"),msg)  

def tests_LDC2List():
    """Teste la fonction LDC2List()"""

    listeVide = creerListeVide() 
    
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2) 
    noeud3 = creerNoeudLDC(3,noeud2,None)
    setSuivantLDC(noeud2,noeud3)   
    ldc = [noeud0,noeud1,noeud2,noeud3]

    # Pour lever les erreurs d'assertions
    ldcKO = ['string',noeud1,noeud2] 

    myTestsTitle("LDC2List()")    
    print("Liste Vide                     : ",listeVide)        
    print("LDC2List(listeVide)      : ",LDC2List(listeVide))
    print(sepSymb("-"))    
    print("Liste ldc                     : ",ldc)        
    print("LDC2List(ldc)      : ",LDC2List(ldc))

    # Pour tester les assertions
    myAssertTestsTitle("LDC2List() -> test assertion")    
    try:
        print(warnString("ldcKO           ->"+str(ldcKO)))        
        LDC2List(ldcKO)
    except AssertionError as msg:
        print(warnString("LDC2List(ldcKO) ->"),msg)  

def tests_List2LDC():
    """Teste la fonction list2LDC()"""

    listeVide = []
    
    ldc = [0,1,2,3]

    # Pour lever les erreurs d'assertions
    ldcKO = 'string'
    ldcKObis = [0,'1',2]

    myTestsTitle("LDC2List()")    
    print("Liste Vide                    : ",listeVide)        
    print("list2LDC(listeVide)           : ",list2LDC(listeVide))
    print(sepSymb("-"))    
    print("Liste ldc                     : ",ldc)        
    print("list2LDC(ldc)      : ",list2LDC(ldc))    

    # Pour tester les assertions
    myAssertTestsTitle("lsit2LDC() -> test assertion")    
    try:
        print(warnString("ldcKO              ->"+str(ldcKO)))        
        list2LDC(ldcKO)
    except AssertionError as msg:
        print(warnString("list2LDC(ldcKO)    ->"),msg)
    print(warnString(sepSymb("-"))) 
    try:
        print(warnString("ldcKObis           ->"+str(ldcKObis)))        
        list2LDC(ldcKObis)
    except AssertionError as msg:
        print(warnString("list2LDC(ldcKObis) ->"),msg) 

def tests_MainP1():
    """Gère les tests de la partie 1"""
    # On récupère la date au début du traitement
    start_time = datetime.now()
    os.system("clear")    
    choice = ''
    while choice not in ['1','2','3','4']:
        os.system("clear")
        myTitle("PARTIE 1 - TESTS")               
        choice = input("""
        Quels tests voulez-vous lancer ? 
        ---> 1 : Les définitions
        ---> 2 : Les getters
        ---> 3 : Les setters
        ---> 4 : Les fonctions        
        """)
    os.system("clear")
    if (choice == '1'):        
        tests_Definitions()
    elif (choice == '2'):        
        tests_Getters()
    elif (choice == '3'):
        tests_Setters()
    elif (choice == '4'):
        functionChoice = ''
        while functionChoice not in ['1','2','3','4','5','6','7','8','9','10','11','12']:
            os.system("clear")
            myTitle("PARTIE 1 - TESTS DES FONCTIONS")
            functionChoice = input("""
            Quelle(s) fonction(s) voulez-vous tester ? 
            ---> 1  : longueurLDC()
            ---> 2  : insererDebutLDC()
            ---> 3  : insererFinLDC()
            ---> 4  : supprimerDebutLDC()
            ---> 5  : supprimerFinLDC()
            ---> 6  : accesLDC()
            ---> 7  : insererPlaceLDC()            
            ---> 8  : supprimerPlaceLDC()            
            ---> 9  : supprimerValLDC()  
            ---> 10 : LDC2List()                      
            ---> 11 : list2LDC()                      
            ---> 12 : Toutes les fonctions
            """)
        os.system("clear")
        if functionChoice in str(range(1,11)):
            myTitle("TESTS FONCTIONS")
        if (functionChoice == '1'):
            tests_longueurLDC()
        elif (functionChoice == '2'):
            tests_insererDebutLDC()
        elif (functionChoice == '3'):
            tests_insererFinLDC()
        elif (functionChoice == '4'):
            tests_supprimerDebutLDC()
        elif (functionChoice == '5'):
            tests_supprimerFinLDC()
        elif (functionChoice == '6'):
            tests_accesLDC()
        elif (functionChoice == '7'):
            tests_insererPlaceLDC()
        elif (functionChoice == '8'):
            tests_supprimerPlaceLDC()
        elif (functionChoice == '9'):
            tests_supprimerValLDC()
        elif (functionChoice == '10'):
            tests_LDC2List()
        elif (functionChoice == '11'):
            tests_List2LDC()
        elif (functionChoice == '12'):
            myTitle("TESTS DE TOUTES LES FONCTIONS")               
            tests_longueurLDC()
            tests_insererDebutLDC()
            tests_insererFinLDC()
            tests_supprimerDebutLDC()
            tests_supprimerFinLDC()
            tests_accesLDC()
            tests_insererPlaceLDC()
            tests_supprimerPlaceLDC()
            tests_supprimerValLDC()
            tests_LDC2List()
            tests_List2LDC()

    # On évalue le temps de traitement    
    end_time = datetime.now()    
    # On affiche
    timeShow(end_time-start_time)

    if __name__ == "__main__":
        restart = ''
        while restart not in ['o','n']:
            genericTitle("UN AUTRE TEST ?",'#')
            restart = input("Taper o ou n pour relancer le script --> ")
            if restart == 'o':
                tests_MainP1()

if __name__ == "__main__":
    tests_MainP1()


