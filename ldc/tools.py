#!/usr/bin/python3
#-*- coding: utf8 -*-

# @author : Sébastien LOZANO

"""
Des outils pour des trucs et des machins made in home.
"""
pass

# Pour les commandes systeme
import os

def colorString(s:str,color="\033[0m")->str:
    """Colore le string **s** avec la couleur **color**
    
    Blanc par défaut
    """
    pass
    return color + s + color + "\033[0m" # retour au blanc à la fin !

def warnString(s:str)->str:
    """Colore un string en jaune."""
    pass
    return colorString(s,"\033[33m")#"\033[33m " + s + " \033[0m"

# Pour homogénéiser le nombre de symboles des séparateurs, laisser un nombre pair !
# On récupère la largeur de la console
nbSymbSep = os.get_terminal_size().columns #160 

def sepSymb(symb:str)->str:
    """Crée une ligne de symboles identiques."""
    pass
    out = ""
    for i in range(nbSymbSep):
        out += symb
    return out

def sepEqual(s:str)->str:
    """Crée une ligne avec un texte centré entouré de symboles =."""
    pass
    out=""
    if ((nbSymbSep-len(s)) % 2 == 0):
        nbEqualGauche = (nbSymbSep-len(s))//2 - 1 # espaces à décompter
        nbEqualDroite = (nbSymbSep-len(s))//2 - 1 # espaces à décompter
    else:
        nbEqualGauche = (nbSymbSep-len(s)-1)//2 + 1 - 1 # espaces à décompter
        nbEqualDroite = (nbSymbSep-len(s)-1)//2 - 1     # espaces à décompter

    for i in range(nbEqualGauche):
        out += "="
    out += " "+s+" "
    for i in range(nbEqualDroite):
        out += "="
    return out

def genericTitle(s:str,symb:str,color="\033[0m"):
    """Pour les proicédures d'affichage des titres"""
    pass    
    print(colorString(sepSymb(symb),color))
    print(colorString(sepEqual(s),color))
    print(colorString(sepSymb(symb),color))

def myTitle(s:str):
    """Procedure d'affichage d'un titre en vert gras."""
    pass
    genericTitle(s,"*","\033[1;32m")

def myTestsTitle(s:str):
    """Procedure d'affichage d'un titre en violet gras."""
    pass
    genericTitle(s,"-","\033[1;35m")

def myAssertTestsTitle(s:str):
    """Procedure d'affichage d'un titre pour les tests d'assertions en jaune."""
    genericTitle(s,"+","\033[33m")
    
def timeShow(time:int):
    """Procedure d'affichage du temps de traitement en cyan."""
    pass
    genericTitle("Durée totale de traitement : "+str(time),"=","\033[36m")
    # print(colorString(sepSymb("="),"\033[36m"))
    # print(colorString("  Durée totale de traitement : "+str(time),"\033[36m"))        
    # print(colorString(sepSymb("="),"\033[36m"))

if __name__ == "__main__":
    os.system("clear")
    print(warnString("Test fonction colorString()"))
    print(sepSymb('-'))
    print(sepEqual("foo"))
    print(sepEqual("barbar"))
    