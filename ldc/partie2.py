#!/usr/bin/python3
#-*- coding: utf8 -*-

# @author : Sébastien LOZANO

"""
Toutes les méthodes permettant de travailler sur l'ordre des listes doublement chainées
"""
pass

# Pour mesurer le temps de traitement du script
from datetime import datetime
# Pour les commandes systeme
import os

from partie1 import *

#############################################################################################
#              FONCTIONS
#############################################################################################
def estOrdonneeLDC(ldc:list)->bool:
    """Renvoie True si la liste doublement chainée **ldc** est croissante au sens large.

    Doublons possibles.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.

    **Sorties**

        * Un booléen.
    
    """
    pass
    assert (ldc is not None),warnString("La liste est vide, on ne peut pas statuer sur l'ordre de ses élements.")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC    
    # On transforme la liste doublement chainee en liste Python
    ldc2listPython = LDC2List(ldc)
    myBool = False
    if ldc2listPython == sorted(ldc2listPython):
        myBool = True
    else:
        myBool = False
    return myBool

def triLDC(ldc:list)->list:
    """Renvoie la liste doublement chainée **ldc** triée dans l'ordre croissant au sens large.

    Doublons possibles.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.

    **Sorties**

        * la liste doublement chainée triée dans l'ordre croissant.
    
    """
    pass
    assert (ldc is not None),warnString("La liste est vide, on ne peut pas trier ses élements.")
    assert (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC    
    # On transforme la liste doublement chainee en liste Python
    ldc2listPython = LDC2List(ldc)
    # On trie la liste
    ldc2listPython.sort()
    # On retransforme en liste doublement chainée
    ldc = list2LDC(ldc2listPython)
    return ldc

def insereOrdreLDC(ldc:list,val:int)->list:
    """Insère la valeur **val** à sa place dans la liste doublement chainée **ldc** croissante, demanière à ce qu'elle reste croissante au sens large.

    Doublons possibles.
    
    **Paramètres**
    
        * ldc -- liste doublement chainée.

    **Sorties**

        * la liste doublement chainée triée dans l'ordre croissant.
    
    """
    pass    
    assert (ldc is None) or estOrdonneeLDC(ldc),warnString(str(LDC2List(ldc))+" devrait être ordonnée")
    assert (ldc is None) or (all(estNoeudLDC(noeud) for noeud in ldc)) # msg issu de estNoeudLDC    
    assert isinstance(val,int),warnString(str(val)+" devrait être un entier")
    
    if ldc is None:
        ldc = []
        ldc = creerNoeudLDC(val,None,None)
    else:
        # On transforme la liste doublement chainee en liste Python
        ldc2listPython = LDC2List(ldc)
        i = 0 # pourl'indice courant
        while ldc2listPython[i]<val:
            i += 1
        # Quand on sort de la boucle on a l'indice d'insertion
        ldc2listPython.insert(i,val)
        ldc = list2LDC(ldc2listPython)
    return ldc


#############################################################################################
#              TESTS
#############################################################################################
def tests_estOrdnneeLDC():
    """Teste la fonction estOrdonneeLDC()"""            

    listeVide = creerListeVide()                
    
    # On crée une liste doublement chainée ordonnée
    ldcOrdonnee = list2LDC([0,1,2])
    # On crée une liste doublement chainée ordonnée avec doublons
    ldcOrdonneeDoublons = list2LDC([0,1,1,2])
    # On crée une liste doublement chainée non-ordonnée
    ldcNonOrdonnee = list2LDC([0,2,1])
    # On crée une liste doublement chainée non-ordonnée avec des doublons
    ldcNonOrdonneeDoublons = list2LDC([0,2,1,3,2])

    # Pour lever les erreurs d'assertions
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldcKO = ['string',noeud1,noeud2] 
        
    myTestsTitle("estOrdonneeLDC()")
    print("ldcOrdonnee                              : ",ldcOrdonnee)        
    print("estOrdonneeLDC(ldcOrdonnee)              : ",estOrdonneeLDC(ldcOrdonnee))
    print(sepSymb("-"))
    print("ldcOrdonneedoublons                      : ",ldcOrdonneeDoublons)        
    print("estOrdonneeLDC(ldcOrdonneeDoublons)      : ",estOrdonneeLDC(ldcOrdonneeDoublons))
    print(sepSymb("-"))    
    print("ldcNonOrdonnee                           : ",ldcNonOrdonnee)        
    print("estOrdonneeLDC(ldcNonOrdonnee)           : ",estOrdonneeLDC(ldcNonOrdonnee))
    print(sepSymb("-"))
    print("ldcNonOrdonneeDoublons                   : ",ldcNonOrdonneeDoublons)        
    print("estOrdonneeLDC(ldcNonOrdonneeDoublons    : ",estOrdonneeLDC(ldcNonOrdonneeDoublons))


    # Tests pour lever les erreurs d'assertion
    myAssertTestsTitle("estOrdonneeLDC() -> test des assertions")    
    try:
        print(warnString("listeVide                  ->"+str(listeVide)))
        estOrdonneeLDC(listeVide)
    except AssertionError as msg:
        print(warnString("estOrdonneeLDC(listeVide)  ->"),msg)
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                  ->"+str(ldcKO)))
        estOrdonneeLDC(ldcKO)
    except AssertionError as msg:
        print(warnString("estOrdonneeLDC(ldcKO)  ->"),msg)  

def tests_triLDC():
    """Teste la fonction triLDC()"""            

    listeVide = creerListeVide()                
    
    # On crée une liste doublement chainée ordonnée
    ldcOrdonnee = list2LDC([0,1,2])
    # On crée une liste doublement chainée ordonnée avec doublons
    ldcOrdonneeDoublons = list2LDC([0,1,1,2])
    # On crée une liste doublement chainée non-ordonnée
    ldcNonOrdonnee = list2LDC([0,2,1])
    # On crée une liste doublement chainée non-ordonnée avec des doublons
    ldcNonOrdonneeDoublons = list2LDC([0,2,1,3,2])

    # Pour lever les erreurs d'assertions
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldcKO = ['string',noeud1,noeud2] 
        
    myTestsTitle("triLDC()")
    print("ldcOrdonnee                      : ",ldcOrdonnee)        
    print("triLDC(ldcOrdonnee)              : ",triLDC(ldcOrdonnee))
    print(sepSymb("-"))
    print("ldcOrdonneedoublons              : ",ldcOrdonneeDoublons)        
    print("triLDC(ldcOrdonneeDoublons)      : ",triLDC(ldcOrdonneeDoublons))
    print(sepSymb("-"))    
    print("ldcNonOrdonnee                   : ",ldcNonOrdonnee)        
    print("triLDC(ldcNonOrdonnee)           : ",triLDC(ldcNonOrdonnee))
    print(sepSymb("-"))
    print("ldcNonOrdonneeDoublons           : ",ldcNonOrdonneeDoublons)        
    print("triLDC(ldcNonOrdonneeDoublons    : ",triLDC(ldcNonOrdonneeDoublons))

    # Tests pour lever les erreurs d'assertion    
    myAssertTestsTitle("triLDC() -> test des assertions")  
    try:
        print(warnString("listeVide                  ->"+str(listeVide)))
        triLDC(listeVide)
    except AssertionError as msg:
        print(warnString("triLDC(listeVide)  ->"),msg)    
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                  ->"+str(ldcKO)))
        triLDC(ldcKO)
    except AssertionError as msg:
        print(warnString("triLDC(ldcKO)  ->"),msg)  

def tests_insereOrdreLDC():
    """Teste la fonction insereOrdreLDC()"""            

    listeVide = creerListeVide()                
    
    # On crée une liste doublement chainée ordonnée
    ldcOrdonnee = list2LDC([0,1,2,3,4,5])
    # On crée une liste doublement chainée ordonnée avec doublons
    ldcOrdonneeDoublons = list2LDC([0,1,1,2,2,2,2,4,6])
    # On crée une liste doublement chainée non-ordonnée
    ldcNonOrdonnee = list2LDC([0,2,1])

    # Pour lever les erreurs d'assertions
    # On crée des noeuds pour faire une liste de noeuds
    noeud0 = creerNoeudLDC(0,None,None)
    noeud1 = creerNoeudLDC(1,noeud0,None)
    setSuivantLDC(noeud0,noeud1)    
    noeud2 = creerNoeudLDC(2,noeud1,None)
    setSuivantLDC(noeud1,noeud2)    
    ldcKO = ['string',noeud1,noeud2] 
        
    myTestsTitle("insereOrdreLDC()")
    print("listeVide                            : ",listeVide)
    listeVide = insereOrdreLDC(listeVide,5)        
    print("insereOrdreLDC(listeVide,5)          : ",listeVide)
    print(sepSymb("-"))
    print(colorString("Pour la lisibilité des tests suivants, on affiche le tableau des valeurs et non les listes doublement chainées","\033[31m")) 
    print(sepSymb("-"))
    print("ldcOrdonnee                          : ",LDC2List(ldcOrdonnee))
    ldcOrdonnee = insereOrdreLDC(ldcOrdonnee,3)
    print("insereOrdreLDC(ldcOrdonnee,3)        : ",LDC2List(ldcOrdonnee))
    print(sepSymb("-"))
    print("ldcOrdonneedoublons                  : ",LDC2List(ldcOrdonneeDoublons))        
    ldcOrdonneeDoublons = insereOrdreLDC(ldcOrdonneeDoublons,3)
    print("insereOrdreLDC(ldcOrdonneeDoublons,3): ",LDC2List(ldcOrdonneeDoublons))

    # Tests pour lever les erreurs d'assertion    
    myAssertTestsTitle("insereOrdreLDC() -> test des assertions")
    try:
        print(warnString("ldcOrdonnee                       ->"+str(LDC2List(ldcOrdonnee))))
        insereOrdreLDC(ldcOrdonnee,'foo')
    except AssertionError as msg:
        print(warnString("insereOrdreLDC(ldcOrdonnee,'foo') ->"),msg)    
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcNonOrdonnee                    ->"+str(LDC2List(ldcNonOrdonnee))))
        insereOrdreLDC(ldcNonOrdonnee,5)
    except AssertionError as msg:
        print(warnString("insereOrdreLDC(ldcNonOrdonnee,5)  ->"),msg)    
    print(warnString(sepSymb("-")))
    try:
        print(warnString("ldcKO                             ->"+str(ldcKO)))
        insereOrdreLDC(ldcKO,5)
    except AssertionError as msg:
        print(warnString("insereOrdreLDC(ldcKO,5)           ->"),msg) 

def tests_MainP2():
    """Gère les tests de la partie 2"""
    # On récupère la date au début du traitement
    start_time = datetime.now()
    os.system("clear")    
    choice = ''
    while choice not in ['1','2','3','4']:
        os.system("clear")
        myTitle("PARTIE 2 - TESTS")
        choice = input("""
        Quels tests voulez-vous lancer ? 
        ---> 1 : estOrdonneeLDC()
        ---> 2 : triLDC()
        ---> 3 : insereOrdreLDC()
        ---> 4 : Toutes les fonctions
        """)
    os.system("clear")
    myTitle("TESTS FONCTIONS")
    if (choice == '1'):
        tests_estOrdnneeLDC()
    elif (choice == '2'):
        tests_triLDC()
    elif (choice == '3'):
        tests_insereOrdreLDC()
    elif (choice == '4'):
        tests_estOrdnneeLDC()
        tests_triLDC()
        tests_insereOrdreLDC()

    # On évalue le temps de traitement    
    end_time = datetime.now()    
    # On affiche
    timeShow(end_time-start_time)

    if __name__ == "__main__":
        restart = ''
        while restart not in ['o','n']:
            genericTitle("UN AUTRE TEST ?",'#')
            restart = input("Taper o ou n pour relancer le script --> ")
            if restart == 'o':
                tests_MainP2()    

if __name__ == "__main__":
    tests_MainP2()